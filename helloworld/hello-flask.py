from flask import Flask

app = Flask (__name__)

@app.route('/')
def hello_world():
    
    return "Hello, World!"

@app.route('/bad')
def bad():
    
    return "bad  "+4

@app.route('/bye')
def bye():
    
    return "bye, world!"

@app.route('/user/<userName>')
def input_username(userName):
    
    return "namamu %s"% userName

@app.route('/name/<first>')
@app.route('/name/<first>/<last>')
def name_test(first, last=None):
   
   nama = first +" "+ last if last else first
   return "Hai %s!"% nama

if __name__ == '__main__':
    
    app.run(host='0.0.0.0' , port=200, debug=True)
